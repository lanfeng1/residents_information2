# Generated by Django 2.2.3 on 2020-05-21 15:34

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0015_auto_20200521_2137'),
    ]

    operations = [
        migrations.AlterField(
            model_name='mobile_residents',
            name='date',
            field=models.DateTimeField(blank=True, null=True),
        ),
    ]
