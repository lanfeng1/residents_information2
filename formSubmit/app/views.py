from django.shortcuts import render

from django.forms.models import model_to_dict
from django.http.response import HttpResponse
import json
from django.views.decorators.csrf import csrf_exempt
from django.conf import settings
from app.models import *

MEDIA_URL = settings.MEDIA_URL


# if True:
#     message = {"code":0,"msg":"同步公有云机器成功,刷新页面获取最新数据!"}
# else:
#     message = {"code":1,"msg":"同步公有云机器失败,项目配置中还没有配置[项目云id]"}
# return JsonResponse(message)

def nationals(request):
    if request.method == 'GET':
        data = national.objects.first().content
        return HttpResponse(data)
    # data_list = '/init_data/nationals.json',
    # json_data = open(data_list),
    # json_load = json.load(json_data)
    #
    # with open(data_list,'r') as  data:
    #     parsed_json = json.load(data)
    #
    #     for result in parsed_json['national']:
    #         national.objects.create(
    #             nationalid=result['nationalid'],
    #             title=result['title'],
    #             value=result['value'],
    #             content=result['content'],
    #         )
    # return JsonResponse({'data': data_list, })


@csrf_exempt
def information_form(request):
    if request.method == 'GET':
        context = {}
        data_list = residents_information.objects.filter()
        _d = []
        for i in data_list:
            _d.append(model_to_dict(i))
        context[''] = _d
        return HttpResponse(json.dumps(context), content_type="application/json")

    if request.method == 'POST':
        post_data = request.body.decode()
        _d = json.loads(post_data)
        # s = residents_information.objects.create()
        # s.save()
        #
        #
        # print('....', post_data)
        name = _d.get('name')
        phone = _d.get('phone')
        car = _d.get('car')
        national = _d.get('national')
        born = _d.get('born')
        culture = _d.get('culture')
        marriage = _d.get('marriage')
        community = _d.get('community')
        unit = _d.get('unit')
        crowd = _d.get('crowd')
        door = _d.get('door')
        specialty = _d.get('specialty')
        idcard = _d.get('idcard')
        stay_time = _d.get('stay_time')
        lease = _d.get('lease')
        home = _d.get('home')
        special_populations = _d.get('special_populations')
        face = _d.get('face')
        relation = _d.get('relation')
        units = _d.get('units')
        address = _d.get('address')
        gender = _d.get('gender')
        username = _d.get('username')
        userphone = _d.get('userphone')
        hobby = _d.get('hobby')
        note = _d.get('note')
        volunteer = _d.get('volunteer')

        m = residents_information()

        m.name = name
        m.national = national
        m.phone = phone
        m.born = born
        m.culture = culture
        m.marriage = marriage
        m.community = community
        m.unit = unit
        m.crowd = crowd
        m.address = address
        m.door = door
        m.specialty = specialty
        m.idcard = idcard
        m.stay_time = stay_time
        m.lease = lease
        m.note = note
        m.special_populations = special_populations
        m.gender = gender
        m.home = home
        m.face = face
        m.car = car
        m.relation = relation
        m.units = units
        m.username = username
        m.userphone = userphone
        m.hobby = hobby
        m.volunteer = volunteer
        m.save()
    return HttpResponse(json.dumps({'id': 'OK'}), content_type="application/json")
