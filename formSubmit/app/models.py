from django.db import models

# Create your models here.

class residents_information(models.Model):
    name = models.CharField(max_length=128, blank=True, null=True, verbose_name="姓名")
    phone = models.CharField(max_length=20, blank=True, null=True, verbose_name="手机号")
    idcard = models.CharField(max_length=20, blank=True, null=True, verbose_name="身份证号码")
    gender = models.CharField(max_length=128, blank=True, null=True, verbose_name="性别")
    national = models.CharField(max_length=128, blank=True, null=True, verbose_name="民族")
    born = models.CharField(max_length=128, blank=True, null=True, verbose_name="出生")
    address = models.CharField(max_length=128, blank=True, null=True, verbose_name="综合地址")
    culture = models.CharField(max_length=128, blank=True, null=True, verbose_name="文化程度")
    marriage = models.CharField(max_length=128, blank=True, null=True, verbose_name="婚姻状况")
    relation = models.CharField(max_length=128, blank=True, null=True, verbose_name="与户主关系")
    car = models.CharField(max_length=128, blank=True, null=True, verbose_name="车辆信息")
    special_populations = models.CharField(max_length=128, blank=True, null=True,verbose_name = "特殊人群选择")
    stay_time = models.CharField(max_length=128, blank=True, null=True,verbose_name="入住时间")
    lease = models.CharField(max_length=128, blank=True, null=True,verbose_name="租期")
    community = models.CharField(max_length=128, blank=True, null=True,verbose_name="小区楼栋")
    unit = models.CharField(max_length=128, blank=True, null=True, verbose_name="楼栋单元")
    door = models.CharField(max_length=128, blank=True, null=True, verbose_name="单元门号")
    face = models.CharField(max_length=128, blank=True, null=True,verbose_name = "政治面貌")
    home = models.CharField(max_length=128, blank=True, null=True,verbose_name="户籍所在")
    units = models.CharField(max_length=128, blank=True, null=True,verbose_name = "单位")
    username = models.CharField(max_length=128, blank=True, null=True,verbose_name="户主姓名")
    userphone = models.CharField(max_length=128, blank=True, null=True,verbose_name="户主手机号")
    hobby = models.CharField(max_length=128, blank=True, null=True, verbose_name="爱好特长")
    note = models.TextField(max_length=128, blank=True, null=True, verbose_name="备注")
    volunteer = models.CharField(max_length=128, blank=True, null=True, verbose_name="志愿意向")

    class Meta:
        verbose_name = '居民信息'
        verbose_name_plural = verbose_name

import json
class national(models.Model):
    # nationalid = models.TextField(max_length=128, blank=True, null=True, verbose_name="民族id")
    # title = models.TextField(max_length=128, blank=True, null=True, verbose_name="民族标签")
    # value = models.TextField(max_length=128, blank=True, null=True, verbose_name="民族值")
    content = models.TextField()

    def save(self, *args, **kwargs):
        super(national, self).save(
        )

    class Meta:
        verbose_name = '民族下拉内容'
        verbose_name_plural = verbose_name